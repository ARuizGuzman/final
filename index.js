let express=require('express');
let cors = require('cors');


//Crear una instancia de express
let app=express();
app.use(cors());
let Contestador=require('./Contestador.js');

//Crear una ruta para responder a las peticiones

let miContestador=new Contestador();

app.get('/', miContestador.responder);
app.get('/saludar', miContestador.responderSaludo);
app.post('/insertar', miContestador.insertar);




//Poner a escuchar al servidor en un puerto específico

app.listen(5000);



