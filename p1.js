function initMap(){
    var mymap = L.map('visor').setView([4.747895, -74.031721], 12);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYW5kcmVzcnVpeiIsImEiOiJja2N3aGltbnMwZHVsMnJsbHg2ejVtNXR2In0.EPBFj3ty-HJSxO2_ZX3O5Q', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
     
        }).addTo(mymap);

    var marker = L.marker([4.692526, -74.032552]).addTo(mymap);
    marker.bindPopup("<b>Centro Comercial</b><br>Hacienda Santa Barbara.").openPopup();

    var marker = L.marker([4.767093, -74.043514]).addTo(mymap);
    marker.bindPopup("<b>Cementerio</b><br>Jardines de la Paz").openPopup();

    var marker = L.marker([4.770167, -74.042109]).addTo(mymap);
    marker.bindPopup("<b>Terminal</b><br>de Transporte Salitre Norte").openPopup();

    var marker = L.marker([4.742563, -74.022830]).addTo(mymap);
    marker.bindPopup("<b>Hospital</b><br>Simon Bolivar.").openPopup();

    var marker = L.marker([4.741579, -74.026715]).addTo(mymap);
    marker.bindPopup("<b>Estacion de</b><br>Policia Usaquen.").openPopup();

    var marker = L.marker([4.741271, -74.0314514]).addTo(mymap);
    marker.bindPopup("<b>Fundacion</b><br>Cardio Infantil").openPopup();

    var polygon = L.polygon([

        [4.664470,-74.010228],
        [4.681111,-74.003004],
        [4.694399,-74.012154],
        [4.725238,-74.006838],
        [4.776159,-74.015844],
        [4.804959,-73.998663],
        [4.820964,-74.010547],
        [4.825290,-74.033836],
        [4.686892,-74.056815],

    ]).addTo(mymap); 
    polygon.bindPopup("Localidad Usaquen");
}

function txt1(){
   text="hola mundo";

    this.text;
}