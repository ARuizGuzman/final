function datosFormulario(){

    var comprobacion = false;
    var expReg = /[\s\S]{3}/;		
    var nombre = document.getElementById('nombreFormUsuario').value;
    if(!expReg.test(nombre)){
        alert('Datos incorrectos, el formulario NO se enviará');
        return false;
    }else{
        alert('Datos correctos, el formulario SI se enviará');
        return true;
    }
}